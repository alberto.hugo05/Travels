package net.ahugo.travels.ui.schedule

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.content.Context
import net.ahugo.travels.util.LocationLiveData
import net.ahugo.core.model.BusScheduleView

class ScheduleViewModel : ViewModel() {

    private var location: LocationLiveData? = null
    private val startLineSelected = MutableLiveData<Boolean>()

    internal fun getBusScheduleList(linesDb: String) : List<BusScheduleView> {
        return ScheduleRepository().getNearestStopPerLine(linesDb, location?.value,
                startLineSelected.value ?: true)
    }

    fun getUserLocation(context: Context): LocationLiveData {
        if (location == null) {
            location = LocationLiveData(context)
        }
        return location!!
    }

    fun setStartLineSelected(isSelected: Boolean) {
        startLineSelected.value = isSelected
    }

    fun getStartLineSelected(): LiveData<Boolean> {
        return startLineSelected
    }
}
