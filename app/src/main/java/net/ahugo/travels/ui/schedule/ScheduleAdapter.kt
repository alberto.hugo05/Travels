package net.ahugo.travels.ui.schedule

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_bus_stop.view.*
import net.ahugo.core.model.BusScheduleView
import net.ahugo.travels.databinding.RowBusStopBinding

interface OnItemClick {
    fun onClick(caller: View, position: Int)
}

class ScheduleAdapter(
        val listener: ScheduleFragment.OnInteractionListener
) : ListAdapter<BusScheduleView, BusViewHolder>(BusStopDiff) {

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): BusViewHolder {
        val binding = RowBusStopBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BusViewHolder(binding, object : OnItemClick {
            override fun onClick(caller: View, position: Int) {
                listener.onClick(getItem(position), caller, position)
            }
        })
    }

    override fun onBindViewHolder(holder: BusViewHolder, position: Int) {
        val model = getItem(position)
        holder.bind(model)

        val lineColor = Color.parseColor(model.model.lineColor)
        holder.itemView.line_card.setCardBackgroundColor(ColorStateList.valueOf(lineColor))
    }
}

class BusViewHolder(
        private val item: RowBusStopBinding,
        private val listener: OnItemClick
) : RecyclerView.ViewHolder(item.root), View.OnClickListener {


    fun bind(modelView: net.ahugo.core.model.BusScheduleView) {
        item.data = modelView
        item.invalidateAll()
        itemView.card_container.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        listener.onClick(view, adapterPosition)
    }
}

object BusStopDiff : DiffUtil.ItemCallback<BusScheduleView>() {

    override fun areItemsTheSame(
            oldItem: BusScheduleView,
            newItem: BusScheduleView
    ): Boolean {
        return oldItem.getLineName() == newItem.getLineName()
    }

    override fun areContentsTheSame(
            oldItem: BusScheduleView,
            newItem: BusScheduleView
    ): Boolean {
        return oldItem.getArrivalName() == newItem.getArrivalName() &&
                oldItem.getExpectedTime() == newItem.getExpectedTime()
    }
}