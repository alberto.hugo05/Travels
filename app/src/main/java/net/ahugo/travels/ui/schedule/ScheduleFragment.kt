package net.ahugo.travels.ui.schedule

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Fade
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.schedule_layout.view.*
import net.ahugo.core.model.BusScheduleView
import net.ahugo.travels.FabOnClickListener
import net.ahugo.travels.R
import net.ahugo.travels.util.VerticalSpaceItemDecoration
import net.ahugo.travels.util.dpToPx
import net.ahugo.travels.util.getDb

class ScheduleFragment : Fragment(), FabOnClickListener {

    interface OnInteractionListener {
        fun onClick(item: BusScheduleView, caller: View, position: Int)
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ScheduleViewModel::class.java)
    }
    private val adapter: ScheduleAdapter by lazy { ScheduleAdapter(listener) }

    private val blueStateList by lazy {
        ColorStateList.valueOf(context!!.getColor(R.color.md_blue_A200))
    }
    private val whiteStateList by lazy {
        ColorStateList.valueOf(context!!.getColor(R.color.md_white_1000))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.schedule_layout, container, false)
        addStopsRecyclerView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.title_constraint_layout.setOnApplyWindowInsetsListener { v, insets ->
            v.setPaddingRelative(0, insets.systemWindowInsetTop, 0, 0)
            insets
        }
        ViewCompat.requestApplyInsets(view)

        view.go_button.setOnClickListener {
            selectButton(view.go_button)
            unSelectButton(view.return_button)
            viewModel.setStartLineSelected(true)
        }
        view.return_button.setOnClickListener {
            selectButton(view.return_button)
            unSelectButton(view.go_button)
            viewModel.setStartLineSelected(false)
        }

        initObservables()
        refreshBusScheduleAdapter()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getUserLocation(context!!).observe(viewLifecycleOwner, Observer {
            view?.no_location_bus_stop?.visibility = if (it == null) View.VISIBLE else View.GONE
            refreshBusScheduleAdapter()
        })
    }

    override fun onFabClick(view: View) {

    }

    private fun initObservables() {
        viewModel.getStartLineSelected().observe(viewLifecycleOwner, Observer {
            refreshBusScheduleAdapter()
        })
    }

    private fun selectButton(view: MaterialButton) {
        view.backgroundTintList = blueStateList
        view.strokeColor = blueStateList
        view.iconTint = whiteStateList
        view.setTextColor(whiteStateList)
    }

    private fun unSelectButton(view: MaterialButton) {
        view.backgroundTintList = whiteStateList
        view.strokeColor = blueStateList
        view.iconTint = blueStateList
        view.setTextColor(blueStateList)
    }

    private fun refreshBusScheduleAdapter() {
        val busStopList = viewModel.getBusScheduleList(context!!.getDb())
        when {
            busStopList.isEmpty() -> {
                view?.no_bus_stop?.visibility = View.VISIBLE
            }
            else -> {
                view?.no_bus_stop?.visibility = View.GONE
            }
        }
        adapter.submitList(busStopList)
    }

    private fun addStopsRecyclerView(view: View) {
        view.bus_stops_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(VerticalSpaceItemDecoration(dpToPx(8)))
            adapter = this@ScheduleFragment.adapter
            (itemAnimator as DefaultItemAnimator).run {
                supportsChangeAnimations = false
                addDuration = 160L
                moveDuration = 160L
                changeDuration = 160L
                removeDuration = 120L
            }
        }
    }

    private var listener = object : OnInteractionListener {
        override fun onClick(item: BusScheduleView, caller: View, position: Int) {
            exitTransition = Fade()
            val extras = FragmentNavigatorExtras(
                    caller to getString(R.string.schedule_row_card_to_detailed_card_transition))
            view?.findNavController()?.navigate(
                    R.id.action_scheduleFragment_to_detailedLineFragment,
                    null, null,
                    extras)
        }
    }
}
