package net.ahugo.travels.ui.detailedline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.transition.Slide
import androidx.transition.TransitionInflater
import kotlinx.android.synthetic.main.detailed_line_layout.*
import kotlinx.android.synthetic.main.detailed_line_layout.view.*
import net.ahugo.travels.R
import net.ahugo.travels.util.dpToPx

class DetailedLineFragment : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(DetailedLineViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exitTransition = Slide()
        returnTransition = Slide()
        sharedElementEnterTransition = TransitionInflater.from(context)
                .inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detailed_line_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.root.setOnApplyWindowInsetsListener { v, insets ->
            v.layoutParams = (v.layoutParams as FrameLayout.LayoutParams).apply {
                topMargin = insets.systemWindowInsetTop + dpToPx(8)
            }
            insets
        }
        ViewCompat.requestApplyInsets(view)
        back_button.setOnClickListener { getView()?.findNavController()?.navigateUp() }
    }
}
