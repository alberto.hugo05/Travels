package net.ahugo.travels.ui.schedule

import android.location.Location
import com.google.gson.Gson
import net.ahugo.travels.util.LatLng
import net.ahugo.core.db.LinesDb
import net.ahugo.core.db.ScheduleItem
import net.ahugo.core.db.StartEnd
import net.ahugo.core.db.StopsItem
import net.ahugo.core.model.BusScheduleView
import net.ahugo.core.model.UnsavedBusSchedule
import org.joda.time.DateTime
import org.joda.time.DateTimeComparator
import org.joda.time.format.DateTimeFormat

class ScheduleRepository {

    /**
     * For each line from lines database, give the nearest stop in time and in distance from the user
     *
     * @param linesDb All bus lines
     * @param userLocation current user location
     * @param startLineSelected if we check start line positions or end ones
     *
     * @return list of [BusScheduleView]
     */
    fun getNearestStopPerLine(
            linesDb: String,
            userLocation: Location?,
            startLineSelected: Boolean
    ): List<BusScheduleView> {
        val busStopList = mutableListOf<BusScheduleView>()
        Gson().fromJson(linesDb, LinesDb::class.java).lines?.forEach {
            val busStop = UnsavedBusSchedule()
            busStop.lineColor = it.color
            busStop.lineName = it.name

            // todo make it work even when we don't have user position
            val latLng = if (userLocation != null) {
                LatLng(userLocation.latitude, userLocation.longitude)
            } else {
                LatLng(43.580824, 7.052211)
            }

            val stop = getNearestStopPosition(it.stops, latLng, startLineSelected)
            if (stop != null) {
                val nearestTimeStop = calculateNearestTimeStop(it.stops!!, stop, startLineSelected)
                busStop.expectedTime = nearestTimeStop.first
                busStop.numberOfStopUntilExpectedTime = nearestTimeStop.second
                if (busStop.expectedTime == null) {
                    // No suited bus line were found
                    return@forEach
                }

                // we force distance since it should never be null at this moment
                busStop.distance = computeStopDistance(stop, latLng, startLineSelected)!!.toInt()
                busStop.stopName = stop.stopName
                busStop.arrivalStopName = if (startLineSelected)
                    it.stops?.last()?.stopName else it.stops?.first()?.stopName
                busStopList.add(BusScheduleView(busStop.save()))
            }
        }

        return busStopList.toList()
    }

    /**
     * Get the nearest stop position from the user
     *
     * @param stops bus stops array
     * @param userPos current user location
     * @param startLineSelected if we check start line positions or end ones
     *
     * @return the nearest stop
     */
    private fun getNearestStopPosition(stops: List<StopsItem>?, userPos: LatLng,
                                       startLineSelected: Boolean): StopsItem? {
        var nearestDistance: Double? = null
        var nearestStop: StopsItem? = null

        stops?.forEach {
            val distance = computeStopDistance(it, userPos, startLineSelected) ?: return@forEach
            if (nearestDistance == null || distance < nearestDistance!!) {
                nearestDistance = distance.toDouble()
                nearestStop = it
            }
        }
        return nearestStop
    }

    /**
     * Compute the distance between the stop location and user location
     *
     * @param stopItem bus stop
     * @param userPos current user location
     * @param startLineSelected if we check start line positions or end ones
     *
     * @return distance between stop location and user location
     */
    private fun computeStopDistance(
            stopItem: StopsItem,
            userPos: LatLng,
            startLineSelected: Boolean
    ): Float? {
        val distance = FloatArray(2)
        var latLng: LatLng? = null

        if (startLineSelected && stopItem.start != null) latLng = getStopPosition(stopItem.start!!)
        if (!startLineSelected && stopItem.end != null) latLng = getStopPosition(stopItem.end!!)

        if (latLng != null) {
            Location.distanceBetween(latLng.latitude, latLng.longitude, userPos.latitude,
                    userPos.longitude, distance)
            return distance[0]
        }
        return null
    }

    /**
     * Retrieve stop position from double array
     *
     * @param startEnd array of [Double].
     *  First position is Latitude
     *  Second position is Longitude
     *
     * @return [LatLng] object of bus stop position
     */
    private fun getStopPosition(startEnd: StartEnd): LatLng {
        return LatLng(startEnd.pos[0], startEnd.pos[1])
    }

    /**
     * Calculate nearest time wise stop from the given [stop].
     *
     * If there is a schedule, it is possible that no time is found for the current day,
     *  this will result in not presenting the current line for the user since no bus will come.
     * If no schedule is present, search in [stops] the one before and the one after to calculate
     *  a prevision for the user.
     *
     * @param stops bus stops array
     * @param stop bus stop
     * @param startLineSelected if we check start line positions or end ones
     *
     * @return The nearest stop time wise if there is one
     */
    private fun calculateNearestTimeStop(
            stops: List<StopsItem>,
            stop: StopsItem?,
            startLineSelected: Boolean
    ): Pair<String?, Int?> {

        val nearestTime: String?
        var beforeSize = 0
        val scheduleItem = if (startLineSelected) stop?.start else stop?.end
        if (scheduleItem?.schedule == null) {
            val stopIndex = stops.indexOf(stop)

            var beforeTime: String? = ""
            for (i in stopIndex until stops.size) {
                beforeSize++
                val sI = if (startLineSelected) stops[i].start else stops[i].end
                if (sI?.schedule != null) {
                    beforeTime = getNearestTime(sI.schedule)
                    if (beforeTime != null) break
                }
            }

            //Log.d("MapStopRepository", "time b4 : $beforeTime - time after : $afterTime")
            nearestTime = beforeTime
        } else {
            nearestTime = getNearestTime(scheduleItem.schedule)
        }

        return Pair(nearestTime, beforeSize)
    }

    /**
     * Get the nearest time in [schedules] list
     *
     * @param schedules [ScheduleItem] list of a bus stop
     *
     * @return [String] of the time formatted like 'HH:mm'
     */
    private fun getNearestTime(schedules: List<ScheduleItem>?): String? {
        val dateTime = DateTime()
        val formatter = DateTimeFormat.forPattern("HH:mm")
        val comparator = DateTimeComparator.getTimeOnlyInstance()
        schedules?.forEach { it ->
            if (it.days?.contains(dateTime.dayOfWeek) == true) {
                val sortedTimes = it.times?.map { formatter.parseDateTime(it) }
                        ?.sortedBy { it.millis }
                var nearest = sortedTimes?.find { comparator.compare(it, dateTime) > 0 }
                if (nearest != null) return formatter.print(nearest)
                nearest = sortedTimes?.find { comparator.compare(it, dateTime) <= 0 }
                if (nearest != null) return formatter.print(nearest)
            }
        }
        return null
    }
}