package net.ahugo.travels.util

import android.Manifest
import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 60000
const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

class LocationLiveData(private val context: Context) : LiveData<Location?>() {

    private val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            value = locationResult?.lastLocation
        }
    }

    @SuppressLint("MissingPermission")
    override fun onActive() {

        if (context.hasNotPermission(Manifest.permission.ACCESS_FINE_LOCATION) ||
                context.hasNotPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            value = null
            return
        }

        mFusedLocationClient.lastLocation.addOnSuccessListener {
            value = it
        }

        if (hasActiveObservers()) {
            val locationRequest = LocationRequest.create()
                    .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
                    .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            mFusedLocationClient.requestLocationUpdates(
                    locationRequest, locationCallback, Looper.myLooper())
        }
    }

    override fun onInactive() {
        mFusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun Context.hasNotPermission(permission: String) : Boolean =
        ActivityCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED
}