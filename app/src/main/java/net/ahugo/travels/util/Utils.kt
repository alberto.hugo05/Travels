package net.ahugo.travels.util

import android.content.Context
import android.graphics.Rect
import android.util.TypedValue
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import net.ahugo.travels.R
import java.lang.reflect.Field
import java.lang.reflect.Modifier

fun Context.dpToPx(dpValue: Int) =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue.toFloat(), resources.displayMetrics)
                .toInt()

fun Fragment.dpToPx(dpValue: Int) =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue.toFloat(), resources.displayMetrics)
                .toInt()

fun Context.getDb() = resources.openRawResource(R.raw.bus_database).bufferedReader(Charsets.UTF_8)
        .use { it.readText() }

data class LatLng(
        var latitude: Double,
        var longitude: Double
)

class VerticalSpaceItemDecoration(
        private val verticalSpaceHeight: Int
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State) {
        outRect.top = verticalSpaceHeight
        outRect.left = verticalSpaceHeight
        outRect.right = verticalSpaceHeight
    }
}