package net.ahugo.travels

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.animation.MotionSpec
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import kotlinx.android.synthetic.main.activity_main.*
import net.danlew.android.joda.JodaTimeAndroid
import android.view.*
import com.google.android.gms.location.LocationServices

interface OnPositionPermissionEnabled {
    fun onPositionPermissionEnabled()
}

interface FabOnClickListener {
    fun onFabClick(view: View)
}

const val FINE_LOCATION_PERMISSION_REQUEST = 1

class MainActivity : AppCompatActivity() {

    private lateinit var positionListener: OnPositionPermissionEnabled

    private val viewModel by lazy { ViewModelProviders.of(this).get(MainActivityViewModel::class.java) }
    private val bottomSheetBehavior by lazy { BottomSheetBehavior.from(bottom_sheet) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setStatusBarTransparent()
        setContentView(R.layout.activity_main)

        JodaTimeAndroid.init(this)
        addBottomSheetView()

        // This is a reminder i lost 2h30 of my life to get this
        fab.hideMotionSpec = MotionSpec().let {
            MotionSpec.createFromResource(this, R.animator.fab_hide_motion_spec)
        }
        fab.showMotionSpec = MotionSpec().let {
            MotionSpec.createFromResource(this, R.animator.fab_show_motion_spec)
        }

        viewModel.fabClickHandler.observe(this, Observer { fabClickListener ->
            fab.setOnClickListener { fabClickListener?.onFabClick(it) }
        })
        viewModel.fabVisibility.observe(this, Observer {
            if (it == true) fab.show() else fab.hide()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            FINE_LOCATION_PERMISSION_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    positionListener.onPositionPermissionEnabled()
                }
            }
        }
    }

    private fun addBottomSheetView() {
        bottom_bar.setNavigationOnClickListener {
            bottomSheetBehavior.state = STATE_EXPANDED
            bottom_sheet_dim.animate().alpha(0.5f).setDuration(300).start()
            bottom_sheet_dim.visibility = View.VISIBLE
        }
        bottom_sheet_dim.setOnClickListener { bottomSheetBehavior.state = STATE_COLLAPSED }
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == STATE_COLLAPSED) {
                    bottom_sheet_dim.animate().alpha(0f).setDuration(150)
                            .withEndAction { bottom_sheet_dim.visibility = View.GONE }.start()
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (bottomSheetBehavior.state == STATE_EXPANDED) {
                bottomSheetBehavior.state = STATE_COLLAPSED
                return true
            }
        }
        return super.onKeyUp(keyCode, event)
    }
}

private fun Window.setStatusBarTransparent() {
    decorView.systemUiVisibility =
            (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
}
