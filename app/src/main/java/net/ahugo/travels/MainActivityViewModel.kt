package net.ahugo.travels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

enum class MainActivityFragment { Schedule, DetailedLines, PreferredLines }

class MainActivityViewModel : ViewModel() {

    lateinit var currentVisibleFragment: MainActivityFragment
    var fabClickHandler = MutableLiveData<FabOnClickListener?>()
    val fabVisibility = MutableLiveData<Boolean>()
}