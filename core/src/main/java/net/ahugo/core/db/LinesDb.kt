package net.ahugo.core.db

import com.google.gson.annotations.SerializedName

data class ScheduleItem(@SerializedName("times")
                        val times: List<String>?,
                        @SerializedName("days")
                        val days: List<Int>?)

data class StopsItem(@SerializedName("start")
                     val start: StartEnd?,
                     @SerializedName("end")
                     val end: StartEnd?,
                     @SerializedName("stop_name")
                     val stopName: String = "",
                     @SerializedName("order")
                     val order: Int = 0)

data class LinesItem(@SerializedName("color")
                     val color: String = "",
                     @SerializedName("name")
                     val name: String = "",
                     @SerializedName("stops")
                     val stops: List<StopsItem>?,
                     @SerializedName("line_id")
                     val lineId: String = "")

data class StartEnd(@SerializedName("schedule")
               val schedule: List<ScheduleItem>?,
                    @SerializedName("pos")
               val pos: List<Double>)

data class LinesDb(@SerializedName("lines")
                 val lines: List<LinesItem>?)