package net.ahugo.core.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

data class BusSchedule(
        var lineName: String,
        var lineColor: String,
        var stopName: String,
        var arrivalStopName: String,
        var expectedTime: String,
        var numberOfStopUntilExpectedTime: Int? = null,
        var distance: Int)

data class UnsavedBusSchedule(
        var lineName: String? = null,
        var lineColor: String? = null,
        var stopName: String? = null,
        var arrivalStopName: String? = null,
        var expectedTime: String? = null,
        var numberOfStopUntilExpectedTime: Int? = null,
        var distance: Int? = null) {

    fun save() : BusSchedule {
        return BusSchedule(lineName!!, lineColor!!, stopName!!, arrivalStopName!!, expectedTime!!,
                numberOfStopUntilExpectedTime, distance!!)
    }
}

class BusScheduleView(val model: BusSchedule) : BaseObservable() {

    @Bindable fun getLineName() : String { return model.lineName }
    @Bindable fun getStopName() : String { return model.stopName.toLowerCase().capitalize() }
    @Bindable fun getArrivalName() : String { return model.arrivalStopName.toLowerCase().capitalize() }
    @Bindable fun getExpectedTime() : String { return model.expectedTime }

    @Bindable fun getNumberOfStopUntilExpectedTime() : String {
        return if (model.numberOfStopUntilExpectedTime != null)
            "+" + model.numberOfStopUntilExpectedTime?.toString()
        else
            ""
    }

    @Bindable fun getDistance() : String {
        return if (model.distance > 1000)
            (model.distance/1000).toString() + "Km"
        else
            (model.distance).toString() + "m"
    }
}